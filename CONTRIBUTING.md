# How to contribute
### Suggestions
To provide suggestions, simply comment on the project and I will (probably) look at it.
### Issues
To point out issues, submit via the issue tracker.
### Pull Requests
I probably won't accept any requests due to this being a school project, but people are welcome to submit regardless.