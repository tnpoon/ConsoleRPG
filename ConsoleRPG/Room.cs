﻿using System;
using System.Collections.Generic;

namespace ConsoleRPG
{
    [Serializable()]
    public class Room
    {
        public String RoomTitle { get; set; } 
        public String RoomDescription { get; set; }
        public List<Exit> Exits { get; set; }
        public List<Item> RoomItems { get; set; }

        //Constructors
        public Room()
        {
            RoomTitle = null;
            RoomDescription = null;
            Exits = new List<Exit>();
            RoomItems = new List<Item>();
        }

        public Room(string title, string desc, List<Exit> exits, List<Item> items)
        {
            RoomTitle = title;
            RoomDescription = desc;
            Exits = exits;
            RoomItems = items;
        }

        //Returns room title
        public String GetTitle()
        {
            return RoomTitle;
        }

        //Sets room title
        public void SetTitle(String roomTitle)
        {
            RoomTitle = roomTitle;
        }
        
        //Adds exit to room
        public void AddExit (Exit exit)
        {
            if (Exits == null)
            {
                Exits = new List<Exit> { exit };
            }
            else
            {
                Exits.Add(exit);
            }            
        }

        //Removes exit from room
        public void RemoveExit(Exit exit)
        {
            if (Exits.Contains(exit))
            {
                Exits.Remove(exit);
            }
        }

        //Prints basic information about room, also prints exits and items in room
        public void PrintDescription()
        {
            Console.WriteLine("\nYou are currently in: " + RoomTitle);
            Console.WriteLine(RoomDescription);
            if (Exits == null)
            {
                Console.WriteLine("There are no exits.");
            }
            else
            {
                Console.WriteLine("There are exits: ");
                foreach (var item in Exits)
                {
                    if (item.GetType() == typeof(Door))
                    {
                        if (((Door)item).IsLocked)
                        {
                            Console.WriteLine(item + " - Door to " + item.GetLeadsTo().RoomTitle + " (Locked)");
                        }
                        else
                        {
                            Console.WriteLine(item + " - Door to " + item.GetLeadsTo().RoomTitle);
                        }                        
                    }
                    else
                    {
                        Console.WriteLine(item + " - " + item.GetLeadsTo().RoomTitle);
                    }                   
                }
            }
            if (RoomItems == null || RoomItems.Count == 0)
            {
                Console.WriteLine("There is nothing here");
            }
            else
            {
                Console.WriteLine("There are items:");
                foreach (var item in RoomItems)
                {
                    Console.WriteLine(item.Name);
                }
            }
        }

        //Prints short description
        public void PrintShortDescription()
        {
            Console.WriteLine("\nYou are currently in: " + RoomTitle);
        }

        //Returns list of exits
        public List<Exit> GetExits()
        {
            if (Exits != null)
            {
                return new List<Exit>(Exits);
            }
            else
            {
                return null;
            }      
        }
    }

    [Serializable()]
    public class EventRoom : Room
    {
        public List<Event> RoomEvent { get; set; }

        //Constructors
        public EventRoom() : base()
        {
            RoomEvent = new List<Event> { };
        }

        public EventRoom(string title, string desc, List<Exit> exits, List<Item> items, List<Event> rmEvent) : base(title, desc, exits, items)
        {
            RoomEvent = rmEvent;
        }
    }
    
}