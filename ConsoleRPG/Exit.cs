﻿using System;

namespace ConsoleRPG
{
    //Class that describes an exit
    [Serializable()]
    public class Exit
    {
        public Room LeadsTo { get; private set; }
        public Directions Direction { get; private set; }

        private string directionName;

        //Constructors
        public Exit()
        {
            Direction = Directions.UNDEFINED;
            LeadsTo = null;
            directionName = Enum.GetName(typeof(Directions), Directions.UNDEFINED) ;
        }

        public Exit (Directions direction, Room leadsTo)
        {
            Direction = direction;
            LeadsTo = leadsTo;
            directionName = Enum.GetName(typeof(Directions), direction);
        }

        //Returns direction name
        public override string ToString()
        {
            return directionName.ToString(); ;
        }

        //Sets direction 
        public void SetDirection(Directions direction)
        {
            this.Direction = direction;
            directionName = Enum.GetName(typeof(Directions), direction);
        }

        //Returns direction name
        public String GetDirectionName()
        {
            return directionName;
        }

        //Sets leads to
        public void SetLeadsTo(Room leadsTo)
        {
            LeadsTo = leadsTo;
        }
        
        //Gets leads to
        public Room GetLeadsTo()
        {
            return LeadsTo;
        }
    }

    [Serializable()]
    public class Door : Exit
    {
        public bool IsLocked { get; private set; }
        public Item KeyItem { get; private set; }

        //Constructors
        public Door() : base()
        {
            IsLocked = false;
            KeyItem = null;
        }

        public Door(Directions direction, Room leadsTo, bool isLocked, Item keyItem) : base(direction, leadsTo)
        {
            IsLocked = isLocked;
            KeyItem = keyItem;
        }

        //Below: changes state of IsLocked
        public void Lock()
        {
            IsLocked = true;
        }

        public void Unlock()
        {
            IsLocked = false;
        }

        //Sets key to item
        public void SetKey(Item key)
        {
            KeyItem = key;
        }
        
    }
}