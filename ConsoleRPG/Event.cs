﻿using System;

namespace ConsoleRPG
{
    [Serializable()]
    public abstract class Event
    {
        //Properties
        public abstract object EventInput { get; set; }
        public abstract string EventDescription { get; set; }
        public abstract int EventCount { get; set; }
        public abstract bool EventState { get; set; }

        //Constructors
        public Event()
        {
            EventInput = null;
            EventDescription = String.Empty;
            EventCount = 0;
            EventState = true;
        } 

        public Event(object input, string description, int count, bool state)
        {
            EventInput = input;
            EventDescription = description;
            EventCount = count;
            EventState = state;
        }

        public abstract void Run();
    }

    [Serializable()]
    public class RemoveHPEvent : Event
    {
        //Event to fire a trap that damages the player's HP.
        public override object EventInput { get; set; }
        public override string EventDescription { get; set; }
        public override int EventCount { get; set; }
        public override bool EventState { get; set; }

        public RemoveHPEvent() : base() 
        {
        }
        public RemoveHPEvent(string description, int count) : base(null, description, count, true) { }

        public override void Run()
        {
            Program.game._player.HP -= EventCount;
            Console.Write("As you enter the room, a " + EventDescription + " trap springs and your HP is damaged by " + EventCount + " points!");
            EventState = false;           
        }
    }

    [Serializable()]
    public class RemoveItemEvent : Event
    {
        public override object EventInput { get; set; }
        public override string EventDescription { get; set; }
        public override int EventCount { get; set; }
        public override bool EventState { get; set; }

        //Constructors
        public RemoveItemEvent() : base() { }
        public RemoveItemEvent(Item input) : base(input, null, 0, true) { }

        public override void Run()
        {
            //Loops through items in inventory, if item matches input, remove item from player inventory.
            foreach (Item item in Program.game._player.Inventory)
            {
                if (item.Name == ((Item)EventInput).Name)
                {
                    Program.game._player.Inventory.Remove(item);
                    Console.WriteLine(((Item)EventInput).Name + " has been removed from your inventory!");
                    EventState = false;                   
                }
            }
        }
    }

    [Serializable()]
    public class AddItemEvent : Event
    {
        public override object EventInput { get; set; }
        public override string EventDescription { get; set; }
        public override int EventCount { get; set; }
        public override bool EventState { get; set; }

        //Constructors
        public AddItemEvent() : base() { }
        public AddItemEvent(Item input) : base(input, null, 0, true) { }

        //Loops through items in inventory, if item matches input, add item to player inventory.
        public override void Run()
        {
            Program.game._player.Inventory.Add(((Item)EventInput));
            Console.WriteLine(((Item)EventInput).Name + " has been added to your inventory!");
            EventState = false;
        }
    }

    [Serializable()]
    public class AddHPEvent : Event
    {
        public override object EventInput { get; set; }
        public override string EventDescription { get; set; }
        public override int EventCount { get; set; }
        public override bool EventState { get; set; }
        
        //Constructors
        public AddHPEvent() : base() {}
        public AddHPEvent(int count) : base(null, null, count, true) { }

        //Adds [count] HP to player
        public override void Run()
        {
            Program.game._player.HP += EventCount;
            Console.WriteLine("You have been healed by " + EventCount + "HP!");
            EventState = false;
        }
    }

    [Serializable()]
    public class ExpositionEvent : Event
    {
        public override object EventInput { get; set; }
        public override string EventDescription { get; set; }
        public override int EventCount { get; set; }
        public override bool EventState { get; set; }

        //Constructors
        public ExpositionEvent() : base() { }
        public ExpositionEvent(string description) : base(null, description, 0, true) { }

        //Prints text
        public override void Run()
        {
            Console.WriteLine();
            Console.WriteLine(EventDescription);
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            EventState = false;
        }
    }

    [Serializable()]
    public class TeleportEvent : Event
    {
        public override object EventInput { get; set; }
        public override string EventDescription { get; set; }
        public override int EventCount { get; set; }
        public override bool EventState { get; set; }

        public TeleportEvent() : base() { }
        public TeleportEvent(Room input, string description) : base(input, description, 0, true) { }

        //Changes currentroom to input
        public override void Run()
        {
            Console.WriteLine(EventDescription + " " + ((Room)EventInput).RoomTitle + "!");
            Program.game.currentRoom = ((Room)EventInput);
            EventState = false;
        }

    }


    [Serializable()]
    public class GameOverEvent : Event
    {
        public override object EventInput { get; set; }
        public override string EventDescription { get; set; }
        public override int EventCount { get; set; }
        public override bool EventState { get; set; }

        public GameOverEvent() : base() { }
        public GameOverEvent(string description) : base(null, description, 0, true) { }

        //Writes a message then exits the game
        public override void Run()
        {
            Console.WriteLine();
            Console.WriteLine(EventDescription);
            Console.ReadLine();
            EventState = false;
            Environment.Exit(0);     
        }
    }

    [Serializable()]
    public class AddScoreEvent : Event
    {
        public override object EventInput { get; set; }
        public override string EventDescription { get; set; }
        public override int EventCount { get; set; }
        public override bool EventState { get; set; }

        public AddScoreEvent() : base() { }
        public AddScoreEvent(int count) : base(null, null, count, true) { }

        //Adds [count] score
        public override void Run()
        {
            Program.game.score += EventCount;
            EventState = false;
        }
    }

    [Serializable()]
    public class LockDoorEvent : Event
    {
        public override object EventInput { get; set; }
        public override string EventDescription { get; set; }
        public override int EventCount { get; set; }
        public override bool EventState { get; set; }

        public LockDoorEvent() : base() { }
        public LockDoorEvent(Room input) : base(input, null, 0, true) { }

        //Input a destination, locks the door going to said destination (if one exists)
        public override void Run()
        {
            foreach (Exit item in Program.game.currentRoom.Exits)
            {
                if (item.GetType() == typeof(Door) & ((Room)EventInput).RoomTitle == item.GetLeadsTo().RoomTitle)
                {
                    ((Door)item).Lock();
                    Console.WriteLine("As you enter the room, the door behind you clicks shut.");
                }
            }
            Program.game.score += EventCount;
            EventState = false;
        }
    }

    [Serializable()]
    public class UnlockDoorEvent : Event
    {
        public override object EventInput { get; set; }
        public override string EventDescription { get; set; }
        public override int EventCount { get; set; }
        public override bool EventState { get; set; }

        public UnlockDoorEvent() : base() { }
        public UnlockDoorEvent(Room input) : base(input, null, 0, true) { }

        //Input a destination, unlocks the door going to said destination (if one exists)
        public override void Run()
        {
            foreach (Exit item in Program.game.currentRoom.Exits)
            {
                if (item.GetType() == typeof(Door) & ((Door)EventInput).GetLeadsTo().RoomTitle == item.GetLeadsTo().RoomTitle)
                {
                    ((Door)item).Unlock();
                }
            }
            Program.game.score += EventCount;
            EventState = false;
        }
    }


}
