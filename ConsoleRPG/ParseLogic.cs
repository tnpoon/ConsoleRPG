﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleRPG
{
    class ParseCommand
    {
        public static ICommandBase CommandInstance { get; set; }

        public static void Parse(string command)
        {
            //Split each sentence into words in put in list
            string[] tokens = command.ToUpper().Split(' ');
            //Exclude first word and set as parameters
            object[] parameters = tokens.Skip(1).ToArray();
            
            //Get all classes in namespace which has the ICommandBase type and put into list
            List<Type> commandTypes = System.Reflection.Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .Where(t => typeof(ICommandBase).IsAssignableFrom(t))
                .ToList();
            
            //If the command only has one word and it is a cardinal direction, go to the direction
            if (parameters.Length == 0 & Enum.IsDefined(typeof(Directions), tokens[0].ToUpper()))
            {
                CommandInstance = new Go()
                {
                    Parameters = new object[] { tokens[0] }
                };
                CommandInstance.Run();
            }
            else
            {
                //Loop for each command
                for (int i = 0; i < commandTypes.Count; i++)
                {
                    //If token matches a command, initialize instance of command and execute it, passing parameters.
                    if (commandTypes[i].Name.ToLower() == tokens[0].ToLower() & commandTypes[i].Name.ToLower() != "icommandbase" & commandTypes[i].Name.ToLower() != "internalcommand")
                    {
                        CommandInstance = (ICommandBase)Activator.CreateInstance(commandTypes[i]);
                        CommandInstance.Parameters = parameters;
                        CommandInstance.Run();
                        break;
                    }
                    //Else if it is the last loop, show error message.
                    else if (i == commandTypes.Count - 1)
                    {
                        Console.WriteLine("I don't know what you're trying to do. Type HELP for a list of commands.");
                    }
                }
            }
            
        }
    }
}
