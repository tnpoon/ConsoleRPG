﻿using System;
using System.Collections.Generic;

namespace ConsoleRPG
{
    [Serializable()]
    public class Player
    {
        //Initilizes the differnet properties a player can have
        public string Name { get; set; }
        public string Gender { get; set; }
        public string HairColour { get; set; }
        public string EyeColour { get; set; }
        public int Height { get; set; }
        public int Age { get; set; }
        public int HP { get; set; }
        //Initilizes inventory
        public List<Item> Inventory { get; private set; }

        //Constructor
        public Player()
        {
            Inventory = new List<Item>();
            HP = 10;
        }

        //Searches inventory for items and prints them, if the item is a container, it also prints out its contents (if it is open)
        public void PrintInventory()
        {
            if (Inventory.Count > 0)
            {
                Console.WriteLine("You are holding these items:");
                foreach (Item item in Inventory)
                {
                    if (item.GetType() == typeof(Container))
                    {
                        if (((Container)item).IsOpen)
                        {
                            Console.WriteLine(String.Format("{0}\n({1})", item.Name, item.Description));
                            foreach (var conItem in ((Container)item).ContainedItems)
                            {
                                Console.WriteLine(String.Format("   {0}\n   ({1})", conItem.Name, conItem.Description));
                            }
                        }
                        else
                        {
                            Console.WriteLine(String.Format("{0} (Closed)\n({1})", item.Name, item.Description));
                        }
                        
                    }
                    else
                    {
                        Console.WriteLine(String.Format("{0}\n({1})", item.Name, item.Description));
                    }                    
                }
            }
            else
            {
                Console.WriteLine("You are not holding any items.");
            }
            
        }

        //Adds item to player's inventory and prints message
        public void AddItem(Item item)
        {
                Inventory.Add(item);
                Console.WriteLine(String.Format("You have picked up {0}\n({1})", item.Name, item.Description));          
        }

        //Removes item to player's inventory and prints message
        public void RemoveItem(Item item)
        {
            if (Inventory.Contains(item))
            {
                Inventory.Remove(item);
                Console.WriteLine(String.Format("You have dropped {0}\n({1})", item.Name, item.Description));
            }
        }

        //Prints a description of the player using the player's properties. Also prints inventory
        public void PrintDescription()
        {
            Console.WriteLine(String.Format("Your name is {0}, you are {1} years old and you are {2}. You hair is {3}, your eyes are {4} and you are {5}cm tall, and you currently have {6} HP.", 
                Name, Age, Gender.ToLower(), HairColour.ToLower(), EyeColour.ToLower(), Height, HP));
            PrintInventory();
        }

    }
    
}