﻿using System;
using System.Globalization;

namespace ConsoleRPG
{
    [Serializable()]
    class Game
    {
        public Room currentRoom;
        //Declare Rooms
        Room atrium, dropRoom, pitfall, puzzleCentre, leftTrapRoom, rightTrapRoom, blueKeyRoom, redKeyRoom, corridor1,
            corridor2, dudRoom1, dudRoom2, dudRoom3, prizeRoom, gameOverRoom;
        //Declare Player
        public Player _player;
        //Declare Score
        public int score;

        public void Run()
        {
            //If game has not been already initialized (not been loaded), run load select           
            if (Program.IfInitilize)
            {
                Console.Title = "Adventurer Training Centre 1.0.1 | LOAD SELECT";
                LoadSelector();              
            }  
                               
            currentRoom.PrintDescription();

            //Main game loop
            while (true)
            {
                Console.Title = String.Format("Adventurer Training Centre 1.0.1 | {2} | Score: {0} | HP: {1}", score, _player.HP, currentRoom.GetTitle());
                CheckStatus();
                Console.Write("\n>");
                ParseCommand.Parse(Console.ReadLine());
            }
        }
        
        void LoadSelector()
        {
            //If player selects 1, initilize and run character creation, else load game from save.dat
            Console.WriteLine("Press (1) to start a new game or press (2) to load a save game:");
            do
            {
                string selection = Console.ReadLine();
                if (selection == "1")
                {
                    //Say welcome message 
                    CreateCharacter();  
                    Initilize();                                   
                    return;
                }
                else if (selection == "2")
                {
                    try
                    {
                        SaveLoad.Load(ref Program.game);
                        Program.IfInitilize = false;
                        Program.game.Run();
                        return;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Cannot load file! Please choose another option!");
                    }
                }
            } while (true);           
        }
        
        //Game script goes here
        void Initilize()
        {
            //Initialize Score
            score = 0;

            //Initilize Rooms
            atrium = new Room();
            dropRoom = new EventRoom();
            pitfall = new Room();
            puzzleCentre = new EventRoom();
            leftTrapRoom = new EventRoom();
            rightTrapRoom = new EventRoom();
            blueKeyRoom = new EventRoom();
            redKeyRoom = new EventRoom();
            corridor1 = new Room();
            corridor2 = new Room();
            dudRoom1 = new Room();
            dudRoom2 = new Room();
            dudRoom3 = new Room();
            prizeRoom = new EventRoom();
            gameOverRoom = new EventRoom();

            //Initialize Items
            var rope = new Item("Climbing Rope", "Maybe you can climb yourway to freedom?");
            var greenKey = new Item("Green Key", "It opens something... I think.");
            var blueKey = new Item("Blue Key", "A beautiful key made of lapis lazuli.");
            var redkey = new Item("Red Key", "A beautiful key made of ruby. Looks like it opens something important.");
            var potion = new Potion("Health Potion", "Heals the player 10HP.", 10);
            var superPotion = new Potion("Super Potion", "Heals the player 100HP.", 100);
            var medallion = new Item("ATC Medallion", "Proof that you are a half-decent advernturer.");
            var note = new Document("Note", "A note given to you by an ATC instructor", 
                @"Welcome to the Adventurer Training Centre! You have done it! After weeks of grueling training under our patent pending adventurer instruction programme, you have managed to reach our programme's final test: The ATC Proving Grounds.

In this trial, you will be using your knowledge to nagivate a treacherous series of rooms filled with, danger, excitement and even DEATH. If you do manage to complete the course, you will recieve an ATC Medallion that is forged by the director himself. However if you don't... Let's say that you wouldn't want to know what happens.

Anyways! Before you go on to your death *cough* excuse me--/proving trial/, here are some tips so you hopefully don't kill yourself:
Navigate to rooms by using the GO command (e.g. GO NORTH)
Open locked doors by using the (you guessed it) OPEN command with a key (e.g. OPEN DOOR WITH KEY)
Use items such as potions by using the USE command (e.g. USE POTION)
Save game by using SAVE command. Load using LOAD command. SAVE and LOAD often!
Type HELP for more commands
p.s. Use the potions (you'll know what I mean later)");
            var postitNote = new Document("Post-It Note", "Disclaimer: This is a product placement sponsered by 3M.",
                String.Format(@"Dear {0},
I would like to congradulate you on behalf of all of the staff for completing the ATC Proving Grounds. The road to being a master adventurer is long buit I am sure this trial will greatly add to your treasure trove of experiences and that you are now a better adventurer because of it.

On the pedestal here is an ATC Medallion. Take it. You've earned it. This medallion proves that you are a qualified adventurer and that you can face (almost) any challenge that stands in front of you.

To leave the training grounds, simply step into the room in front of you and you will be airlifted back to the main facility. I would like to thank you again from the bottom of my heart.

--
The Director", _player.Name));

            //Set propertires for atrium
            atrium.RoomTitle = "ATC Trial Room Atrium";
            atrium.RoomDescription = "The atrium of the trial room. The room is pretty spartan, with concrete walls all around and a unlocked door in front of you. You seem to have been dropped off from the top, as the ceiling is open to the elements." +
                "In your pocket is a note given to you by an ATC instructor.";
            atrium.AddExit(new Exit(Directions.NORTH, dropRoom));
    
            //Set properties for droproom;
            dropRoom.RoomTitle = "Drop Room";
            dropRoom.RoomDescription = "Curses! Who puts a trapdoor in the floor of a room?";
            dropRoom.AddExit(new Door(Directions.SOUTH, atrium, false, null));
            dropRoom.AddExit(new Exit(Directions.NORTH, puzzleCentre));
            ((EventRoom)dropRoom).RoomEvent.Add(new LockDoorEvent(atrium));
            ((EventRoom)dropRoom).RoomEvent.Add(new TeleportEvent(pitfall, "You have fallen into a"));

            //Set properties for pitfall
            pitfall.RoomTitle = "Pit";
            pitfall.RoomDescription = "A deep pit that was dug into the ground, the walls are lined with ceremic tiles and you can't seem to be able to grip it. Is this the end?";
            pitfall.AddExit(new Door(Directions.UP, dropRoom, true, rope));
            pitfall.RoomItems.Add(rope);

            //Set properties for puzzle centre
            puzzleCentre.RoomTitle = "Puzzle Centre";
            puzzleCentre.RoomDescription = "A large domed room with doors on every side. In front of you is a locked door that is painted red and to your sides are two ordinary looking rooms. For some reason, the doors on either side have 'DANGER' written on them...";
            puzzleCentre.AddExit(new Door(Directions.NORTH, prizeRoom, true, redkey));
            puzzleCentre.AddExit(new Exit(Directions.EAST, rightTrapRoom));
            puzzleCentre.AddExit(new Exit(Directions.WEST, leftTrapRoom));
            puzzleCentre.AddExit(new Door(Directions.SOUTH, dropRoom, false, null));
            ((EventRoom)puzzleCentre).RoomEvent.Add(new LockDoorEvent(dropRoom));
            puzzleCentre.RoomItems.Add(greenKey);
            puzzleCentre.RoomItems.Add(potion);

            //Set properties for left trap room
            leftTrapRoom.RoomTitle = "Ordinary Room (Left)";
            leftTrapRoom.RoomDescription = "Ouch! Not so ordinary now isn't it";
            leftTrapRoom.AddExit(new Exit(Directions.SOUTH, blueKeyRoom));
            leftTrapRoom.AddExit(new Exit(Directions.EAST, puzzleCentre));
            ((EventRoom)leftTrapRoom).RoomEvent.Add(new RemoveHPEvent("poison gas", 10));

            //Set properties for right trap room
            rightTrapRoom.RoomTitle = "Ordinary Room (Right)";
            rightTrapRoom.RoomDescription = "Ouch! Not so ordinary now isn't it";
            rightTrapRoom.AddExit(new Exit(Directions.NORTHEAST, corridor1));
            rightTrapRoom.AddExit(new Exit(Directions.WEST, puzzleCentre));
            ((EventRoom)rightTrapRoom).RoomEvent.Add(new RemoveHPEvent("flurry of blades", 10));

            //Set properties for blue key room
            blueKeyRoom.RoomTitle = "Key Room";
            blueKeyRoom.RoomDescription = "Hey look! This room contains a key! Maybe this will help you get to the exit.";
            ((EventRoom)blueKeyRoom).RoomEvent.Add(new AddScoreEvent(10));
            blueKeyRoom.AddExit(new Exit(Directions.NORTH, leftTrapRoom));
            blueKeyRoom.RoomItems.Add(blueKey);
            blueKeyRoom.RoomItems.Add(potion);

            //Set properties for corridor 1
            corridor1.RoomTitle = "Corridor";
            corridor1.RoomDescription = "It's... an ordinary corridor. What? Did you expect it to be something else?";
            corridor1.AddExit(new Exit(Directions.SOUTHWEST, rightTrapRoom));
            corridor1.AddExit(new Exit(Directions.EAST, corridor2));
            corridor1.AddExit(new Exit(Directions.NORTH, dudRoom1));

            //Set properties for dudroom1
            dudRoom1.RoomTitle = "Armory";
            dudRoom1.RoomDescription = "A room filled with a variety of plastic and foam weapons. How useful.";
            dudRoom1.AddExit(new Exit(Directions.SOUTH, corridor1));

            //Set properties for corridor 2
            corridor2.RoomTitle = "Corridor";
            corridor2.RoomDescription = "It's... an ordinary corridor. What? Did you expect it to be something else?";
            corridor2.AddExit(new Exit(Directions.NORTHEAST, dudRoom2));
            corridor2.AddExit(new Exit(Directions.WEST, corridor1));
            corridor2.AddExit(new Exit(Directions.EAST, dudRoom3));
            corridor2.AddExit(new Exit(Directions.SOUTH, redKeyRoom));

            //Set properties for dudroom2
            dudRoom2.RoomTitle = "Plushie Room";
            dudRoom2.RoomDescription = "A room overflowing with many kyute plushies. Unicorns, dolphins, cats, Colonel Sanders... Hold on. Is that a Donald Trump plush? Ewwwwww! ";
            dudRoom2.AddExit(new Exit(Directions.SOUTHWEST, corridor2));

            //Set properties for dudroom3
            dudRoom3.RoomTitle = "Torture Chamber";
            dudRoom3.RoomDescription = "A deep, dark, dank room filled with various torture instruments... made of cardboard? What kind of sick joke is this?";
            dudRoom3.AddExit(new Exit(Directions.WEST, corridor2));

            //Set properties for redkeyroom
            redKeyRoom.RoomTitle = "Broom Clost";
            redKeyRoom.RoomDescription = "A small broom closet with brooms (of course) and various cleaning supplies. Wait a second, look! A red key!";
            redKeyRoom.AddExit(new Exit(Directions.NORTH, corridor2));
            redKeyRoom.RoomItems.Add(redkey);
            redKeyRoom.RoomItems.Add(potion);
            ((EventRoom)redKeyRoom).RoomEvent.Add(new AddScoreEvent(50));

            //Set properties for prizeroom
            prizeRoom.RoomTitle = "Prize Room";
            prizeRoom.RoomDescription = "A room with walls made of gold (gold wallpaper)! And what's that on a pedestal in the middle? An ATC Medallion of your very own! Also, there's a post-it note stuck on the medallion";
            prizeRoom.AddExit(new Door(Directions.SOUTH, puzzleCentre, false, null));
            prizeRoom.AddExit(new Exit(Directions.NORTH, gameOverRoom));
            (((EventRoom)prizeRoom)).RoomEvent.Add(new LockDoorEvent(puzzleCentre));
            prizeRoom.RoomItems.Add(medallion);
            prizeRoom.RoomItems.Add(postitNote);

            //Set properties for gameoveroom
            gameOverRoom.RoomTitle = "Exit";
            ((EventRoom)gameOverRoom).RoomEvent.Add(new GameOverEvent(@"Thank you for playing Adventurer Training Centre!
I hope you have enjoyed this short interactive experience and I also hope that you check out the source code at http://gitlab.com/tnpoon/ConsoleRPG.

When I created this game, I was inspired by old text adventure RPGs (especially ones by infocom) such as Zork and Plundered Hearts and since I could make anything for my ISP, I thought it would be cool to make one of these games.

Throughout the coding process, I encountered many challenges (especially with text parsing) but in my opinion it was well worth it considering that I made a text adventure engine from scratch.

Lastly, I would like to thank all the students who helped me test this game and also my teacher Ms. Winger for teaching me C#! Thanks!"));

            /*
            //Sets default player properties
            _player.Name = "Rachel Watson";
            _player.Gender = "Female";
            _player.Height = 140;
            _player.Age = 13;
            _player.HairColour = "Brown";
            _player.EyeColour = "Hasel";
            _player.HP = 10;
            */

            //Add starting items to player
            _player.Inventory.Add(note);

            //Set current room
            currentRoom = atrium;     
        }

        void CreateCharacter()
        {
            //Initialize Player
            _player = new Player();

            Console.WriteLine("Adventurer Training Centre 1.0.1 Bugfix3\nBy Tin Nok Poon\nWelcome adventerer! Please answer these questions to create your character");

            //Set Name
            Console.WriteLine("What is your name?");
            _player.Name = Console.ReadLine();
            Console.WriteLine();

            //Set Gender
            while (true)
            {
                Console.WriteLine("What is your gender?");
                string input = Console.ReadLine().ToUpper();
                if (input == "M" || input == "MALE")
                {
                    _player.Gender = "Male";
                    Console.WriteLine();
                    break;
                }
                else if (input == "F" || input == "FEMALE")
                {
                    _player.Gender = "Female";
                    Console.WriteLine();
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid input!");
                    Console.WriteLine();
                }
            }

            //Set Age
            while (true)
            {
                Console.WriteLine("What is your age?");
                string input = Console.ReadLine();
                if (int.TryParse(input, out int value))
                {
                    if (value > 0)
                    {
                        _player.Age = value;
                        Console.WriteLine();
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Enter an age higher than zero!");
                        Console.WriteLine();
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input!");
                    Console.WriteLine();
                }
            }

            //Set hair colour
            Console.WriteLine("What is the colour of your hair?");
            string hairinput = Console.ReadLine();
            _player.HairColour = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(hairinput.ToLower()); //Set case of first letter of word to upper
            Console.WriteLine();

            //Set eye colour
            Console.WriteLine("What are the colours of your eyes?");
            string eyeinput = Console.ReadLine();
            _player.EyeColour = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(eyeinput.ToLower()); //Set case of first letter of word to upper
            Console.WriteLine();

            //Set Height
            while (true)
            {
                Console.WriteLine("What is your height? (in cm)");
                string input = Console.ReadLine();
                if (int.TryParse(input, out int value))
                {
                    if (value > 0)
                    {
                        _player.Height = value;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Enter an height higher than zero!");
                        Console.WriteLine();
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input!");
                    Console.WriteLine();
                }
            }
        }

        void CheckStatus()
        {
            //Check player HP, if HP <= 0, "kill" player and quit game
            if (_player.HP <= 0)
            {
                Console.WriteLine("\nGAME OVER: You are dead!\nDo you want to load from the previous save?\nPress Y for yes, N for no.");
                do
                {
                    string input = Console.ReadLine();
                    if (input.ToUpper() == "Y")
                    {
                        try
                        {
                            SaveLoad.Load(ref Program.game);
                            Program.IfInitilize = false;
                            Program.game.Run();
                            return;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Cannot load file! Reason: " + e);
                        }
                    }
                    else if (input.ToUpper() == "N")
                    {
                        Environment.Exit(0);
                    }
                    else
                    {
                        Console.WriteLine("Invalid input!");
                    }
                } while (true);               
            }
        }
    }
    
}