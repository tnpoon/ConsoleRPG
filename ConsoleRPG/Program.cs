﻿//ConsoleRPG 1.0.1 Bugfix1 by Tin Nok Poon
//2017-01-18
//An old-school text adventure engine / game
using System;

namespace ConsoleRPG
{
    class Program
    {
        //Creates new game object
        public static Game game = new Game();
        public static bool IfInitilize = true;

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            //Runs game
            game.Run();       
        }

    }

    //Enumeration of all the different possible directions that the player can navigate to
    public enum Directions
    {
        UNDEFINED,
        NORTH,
        SOUTH,
        EAST,
        WEST,
        UP,
        DOWN,
        NORTHEAST,
        NORTHWEST,
        SOUTHEAST,
        SOUTHWEST,
        IN,
        OUT
    };
    
}