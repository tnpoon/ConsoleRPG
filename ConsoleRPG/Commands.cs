﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleRPG
{
    //Interface for all commands
    interface ICommandBase
    {
        object[] Parameters { get; set; }
        void Run();
    }

    //Abstract class for all commands
    abstract class InternalCommand : ICommandBase
    {
        public abstract object[] Parameters { get; set; }
        public abstract void Run();

    }

    class Go : InternalCommand
    {
        public override object[] Parameters { get; set; }

        //Goes to room based on the direction name entered
        public override void Run()
        {
            //If the room has exits, run
            if (Program.game.currentRoom.Exits != null & Program.game.currentRoom.Exits.Count != 0)
            {
                //For each exit in the current room
                foreach (Exit e in Program.game.currentRoom.GetExits())
                {
                    //If params are not empty
                    if (Parameters.Length > 0)
                    {
                        //If direction name matches parameters
                        if (e.GetDirectionName() == Parameters[0].ToString().ToUpper())
                        {
                            //If the exit is a normal exit, go to room and if the room is an event room, run event
                            if (e.GetType() == typeof(Exit))
                            {
                                Program.game.currentRoom = e.GetLeadsTo();
                                if (e.GetLeadsTo().GetType() == typeof(EventRoom))
                                {
                                    foreach (Event ev in ((EventRoom)Program.game.currentRoom).RoomEvent)
                                    {
                                        if (ev.EventState)
                                        {
                                            ev.Run();
                                        }
                                    }                                                                   
                                }
                                Program.game.currentRoom.PrintDescription();
                                return;
                            }
                            //If the exit is a door, go to room if unlock. If room is locked, show error.
                            //if the room is an event room, run event
                            else if (e.GetType() == typeof(Door))
                            {
                                if (((Door)e).IsLocked == true)
                                {
                                    Console.WriteLine("This door is locked! Perhaps you can find a key to unlock it?");
                                }
                                else
                                {
                                    Program.game.currentRoom = e.GetLeadsTo();
                                    foreach (Event ev in ((EventRoom)Program.game.currentRoom).RoomEvent)
                                    {
                                        if (ev.EventState)
                                        {
                                            ev.Run();
                                        }
                                    }
                                    Program.game.currentRoom.PrintDescription();
                                    return;
                                }
                            }
                            else
                            {
                                throw new NotImplementedException();
                            }
                            
                        }
                    } 
                }
            }
            else
            {
                Console.WriteLine("There are no exits.");
                return;
            }

            //If there are no params, show error. If the direction entered is not an exit, show another error.
            if (Parameters.Length == 0)
            {
                Console.WriteLine("This command requires more parameters!\nUsage:\n   go [direction]");
            }
            else if (Enum.IsDefined(typeof(Directions), Parameters[0].ToString().ToUpper()))
            {
                Console.WriteLine("You cannot go " + (string)Parameters[0] + "!");
                return;
            }
        }
    }

    //Prints description of room
    class Look : InternalCommand
    {
        public override object[] Parameters { get; set; }

        public Room CurrentRoom { get; set; }

        public Look()
        {
            CurrentRoom = Program.game.currentRoom;
        }

        public override void Run()
        {
            CurrentRoom.PrintDescription();
            return;
        }
    }

    //Adds item from room to player's inventory
    class Take : InternalCommand
    {
        public override object[] Parameters { get; set; }

        public override void Run()
        {
            string combinedString = String.Join(" ", Parameters);

            //If room has items
            if (Program.game.currentRoom.RoomItems != null)
            {
                if (Program.game.currentRoom.RoomItems.Count != 0)
                {
                    foreach (Item i in Program.game.currentRoom.RoomItems)
                    {
                        //If item name matches params
                        if (i.Name.ToUpper() == combinedString.ToUpper())
                        {
                            //Add item to player inv. Remove item from room items
                            Program.game._player.AddItem(i);
                            Program.game.currentRoom.RoomItems.Remove(i);
                            return;
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("The room has no items.");
                return;
            }
            Console.WriteLine(String.Format("You cannot take {0}!", combinedString));
        }
    }

    //Removes item from player inventory and puts it in room inventory
    class Drop : InternalCommand
    {
        public override object[] Parameters { get; set; }

        public override void Run()
        {
            string combinedString = String.Join(" ", Parameters);

            if (Program.game._player.Inventory != null)
            {
                if (Program.game._player.Inventory.Count != 0)
                {
                    foreach (Item i in Program.game._player.Inventory)
                    {
                        if (i.Name.ToUpper() == combinedString.ToUpper())
                        {
                            Program.game._player.RemoveItem(i);
                            if (Program.game.currentRoom.RoomItems == null)
                            {
                                Program.game.currentRoom.RoomItems = new List<Item> { i };
                            }
                            else
                            {
                                Program.game.currentRoom.RoomItems.Add(i);
                            }          
                            return;
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("You do not have this item in your inventory.");
                return;
            }

            if (Program.game._player.Inventory.Count == 0)
            {
                Console.WriteLine("You do not have anything in your inventory!");
            }
            else
            {
                Console.WriteLine(String.Format("You do not have {0} in your inventory!", combinedString));
            }     
        }
    }

    //Prints description of self
    class Me : InternalCommand
    {
        public override object[] Parameters { get; set; }

        public override void Run()
        {
            Program.game._player.PrintDescription();
        }
    }

    //Exits program
    class Quit : InternalCommand
    {
        public override object[] Parameters { get; set; }

        public override void Run()
        {
            Environment.Exit(0);
        }
    }

    //Shows list of commands
    class Help : InternalCommand
    {
        public override object[] Parameters { get; set; }

        public override void Run()
        {
            //Makes list of all command types
            List<Type> commandTypes = System.Reflection.Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .Where(t => typeof(ICommandBase).IsAssignableFrom(t))
                .ToList();

            Console.WriteLine("These are the commands avaliable:");

            //Prints all commands except base command
            foreach (Type derivedType in commandTypes)
            {
                if (derivedType.Name.ToLower() != "icommandbase" & derivedType.Name.ToLower() != "internalcommand")
                {
                    Console.WriteLine(derivedType.Name);
                }
            }
        }
    }

    class Open : InternalCommand
    {
        public override object[] Parameters { get; set; }

        public override void Run()
        {
            //Joins params and splits them into two parts seperated with "with"
            string combinedCommand = String.Join(" ", Parameters);
            string[] commands = Array.ConvertAll(combinedCommand.Split(new String[] { "WITH" }, StringSplitOptions.None), p => p.Trim());

            //Makes lists of doors and containers in game
            List<Door> doors = new List<Door> { };
            List<Container> containers = new List<Container> { };
            foreach (var item in Program.game.currentRoom.Exits)
            {
                if (item.GetType() == typeof(Door))
                {
                    doors.Add((Door)item);
                }
            }

            foreach (var item in Program.game._player.Inventory)
            {
                if (item is Container)
                {
                    containers.Add((Container)item);
                }
            }

            if (Parameters.Length < 3 & Parameters.Length != 1)
            {
                Console.WriteLine("Invalid command!\nUsage:\n   For doors:\n   open [door] with [object]\n   For containers:\n   open [container]");
            }
            else if (Parameters.Contains("WITH") & (string)Parameters[Parameters.Length - 1] != "WITH" & (string)Parameters[0] != "WITH")
            {   
                //If params is simpily "door"                         
                if (commands[0] == "DOOR")
                {
                    //If there is only one door, unlock door (if player has key item)
                    if (doors.Count == 1)
                    {
                        LockUnlock(commands[1], doors[0]);                  
                    }
                    //Else show error and show doors
                    else if (doors.Count > 1)
                    {
                        Console.WriteLine("Choose which door you want to open:");
                        foreach (var item in doors)
                        {
                            Console.WriteLine(item.LeadsTo.RoomTitle);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Test!");
                    }
                }
                //If a door matches param
                else if (doors.Where(p => p.GetDirectionName().ToUpper() == commands[0]).ToList().Count > 0 ||
                    doors.Where(d => d.LeadsTo.RoomTitle.ToUpper() == commands[0]).ToList().Count > 0)
                {
                    foreach (Door door in doors)
                    {
                        //If door matches door
                        if (commands[0] == door.LeadsTo.RoomTitle.ToUpper() || commands[0] == door.GetDirectionName() & commands[1] != null)
                        {
                            //Unlock / lock door
                            LockUnlock(commands[1], door);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Invalid command!\nUsage:\n   For doors:\n   open [door] with [object]\n   For containers:\n   open [container]");
                }
            }
            //If player is trying to open container
            else if (containers.Where(p => p.Name.ToUpper() == commands[0]).ToList().Count > 0)
            {
                foreach (Container item in containers)
                {
                    //Open container if param matches container name
                    if (item.Name.ToUpper() == commands[0])
                    {
                        if (item.IsOpen)
                        {
                            item.Close();
                            Console.WriteLine(String.Format("{0} closed!", item.Name));
                        }
                        else
                        {
                            item.Open();
                            Console.WriteLine(String.Format("{0} opened!", item.Name));
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Invalid command!\nUsage:\n   For doors:\n   open [door] with [object]\n   For containers:\n   open [container]");
            }
        }

        //Logic for matching items
        private Item MatchItem(string item, List<Item> matchItems)
        {
            foreach (var i in matchItems)
            {
                if (item.ToUpper() == i.Name.ToUpper())
                {
                    return i;
                }
            }
            return null;
        }

        //Logic for matching keys to doors
        private void LockUnlock(string itemName, Door door)
        {
            if (door.KeyItem != null)
            {
                if (MatchItem(itemName, Program.game._player.Inventory) != null )
                {
                    if (MatchItem(itemName, Program.game._player.Inventory).Name == door.KeyItem.Name)
                    {
                        if (door.IsLocked)
                        {
                            door.Unlock();
                            Console.WriteLine("Unlocked Door!");
                        }
                        else
                        {
                            door.Lock();
                            Console.WriteLine("Locked Door!");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid key!");
                    }
                }
                else
                {
                    Console.WriteLine("You do not have " + itemName + " in your inventory!");
                }
            }
            else
            {
                Console.WriteLine("This door cannot be opened!");
            }           
        }
    }

    //Runs the save command
    class Save : InternalCommand
    {
        public override object[] Parameters { get; set; }

        public override void Run()
        {
            SaveLoad.Save(Program.game);         
        }
    }

    //Runs the load command
    class Load : InternalCommand
    {
        public override object[] Parameters { get; set; }

        public override void Run()
        {
            SaveLoad.Load(ref Program.game);
            Program.IfInitilize = false;
            Program.game.Run();
        }
    }

    /*
    //Cheat command to add score
    class AddScore : InternalCommand
    {
        public override object[] Parameters { get; set; }

        public override void Run()
        {
            Program.game.score += 10;
            Console.WriteLine("Current Score: " + Program.game.score);
        }
    }
    */

    //Uses an item
    class Use : InternalCommand
    {
        public override object[] Parameters { get; set; }

        public override void Run()
        {
            String combinedString = String.Join(" ", Parameters);

            //If params matches the name of an item in player's inventory
            foreach (Item item in Program.game._player.Inventory)
            {
                if (combinedString.ToUpper() == item.Name.ToUpper())
                {
                    //If item is potion, use potion
                    if (item is Potion)
                    {
                        ((Potion)item).Use();
                        Program.game._player.Inventory.Remove(item);
                        return;
                    }
                    //If item is document, read document
                    else if (item is Document)
                    {
                        ((Document)item).Read();
                        return;
                    }
                    //Else show error
                    else
                    {
                        Console.WriteLine("You cannot use " + item.Name + "!");
                    }
                }
            }
            Console.WriteLine("Invalid command!\nUsage:\n   use [item]");
        }
    }

    class Read : InternalCommand
    {
        public override object[] Parameters { get; set; }

        public override void Run()
        {
            String combinedString = String.Join(" ", Parameters);

            //If params matches the name of an item in player's inventory
            foreach (Item item in Program.game._player.Inventory)
            {
                if (combinedString.ToUpper() == item.Name.ToUpper())
                {
                    //If item is document, read document
                    if (item is Document)
                    {
                        ((Document)item).Read();
                        return;
                    }
                    //Else show error
                    else
                    {
                        Console.WriteLine("You cannot read " + item.Name + "!");
                    }
                }
            }
            Console.WriteLine("Invalid command!\nUsage:\n   read [document]");
        }
    }

    class Close : Open { }

}
