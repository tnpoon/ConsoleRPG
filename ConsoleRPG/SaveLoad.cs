﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace ConsoleRPG
{

    static class SaveLoad
    {
        public static void Save(Game game)
        {
            //Constructs a stream then constructs a BinaryFormatter and use it to serialize the data to the stream.
            FileStream saveFile = new FileStream("save.dat", FileMode.Create, FileAccess.Write, FileShare.None);         
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                //Serialize the contents of the game into save.dat
                formatter.Serialize(saveFile, game);
                Console.WriteLine("Sucessfully saved to save.dat!");
            }
            catch (SerializationException e)
            {
                //Shows error if above yields an exception
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                //Close save file
                saveFile.Close();
            }
        }

        public static void Load(ref Game game)
        {
            Program.game = null;
            //Open savefile
            FileStream saveFile = new FileStream("save.dat", FileMode.Open, FileAccess.Read, FileShare.Read);

            try
            {
                //Reads data from save file then deserialises it into the current game
                BinaryFormatter formatter = new BinaryFormatter();
                game = (Game) formatter.Deserialize(saveFile);
                Console.WriteLine("Successfully loaded save.dat!");
                GC.Collect();
            }
            catch (SerializationException e)
            {
                //Shows error if above yields an exception
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                //Close save file
                saveFile.Close();
            }
        }
    }
    
}