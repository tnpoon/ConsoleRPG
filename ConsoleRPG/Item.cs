﻿using System;
using System.Collections.Generic;

namespace ConsoleRPG
{
    [Serializable()]
    public class Item
    {
        public string Name { get; set; }
        public string Description { get; set; }

        //Constructor
        public Item(string Name, string Description)
        {
            this.Name = Name;
            this.Description = Description;
        }
    }

    [Serializable()]
    public class Container : Item
    {
        public List<Item> ContainedItems { get; set; }
        public bool IsOpen { get; set; }

        //Constructor
        public Container(string Name, string Description, List<Item> conItems, bool isOpen) : base(Name, Description)
        {
            ContainedItems = conItems;
            IsOpen = isOpen;
        }

        //Opens door
        public void Open()
        {
            IsOpen = true;
        }

        //Closes door
        public void Close()
        {
            IsOpen = false;
        }
    }

    [Serializable()]
    public class Document : Item
    {
        public string Contents { get; set; }

        //Constructor
        public Document(string Name, string Description, string contents) : base(Name, Description)
        {
            Contents = contents;
        }

        //Prints out the document's contents
        public void Read()
        {
            Console.WriteLine(Contents);
        }
    }

    [Serializable()]
    public class Potion : Item
    {
        public int HealCount { get; set; }

        //Constructor
        public Potion(string Name, string Description, int count) : base(Name, Description)
        {
            HealCount = count;
        }

        //Adds [count] HP to the player
        public void Use()
        {
            Program.game._player.HP += HealCount;
            Console.WriteLine("You have been healed by " + HealCount + "HP!");
        }
    }


}