# Console RPG
## By Tin Nok Poon
---
### What is is it?
Console RPG is a project that aims to recreate the RPGs of old in C#. It is created for my computer science ISP (final project). It is an RPG in the style of games such as Zork and will feature dungeon crawling mechanics with rooms that are freely explorable by the player.
By default, the game contains the contents of "Adventurer Training Centre"; a short game that aims to showcase the capabilities of the engine. If you want to write your own game, simply replace the contents of the "Initilize" method in Game.cs.
### How to build
This project uses .NET framework 4.5.2. Simply open the .sln file in Visual Studio then build to exe and run.
### Licence
The project is licenced under the MIT licence, which means that you can copy my code and basically do whatever with it as you include a copy of the licence in your work.